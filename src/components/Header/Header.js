import React from "react";
import TotalUsers from './TotalUsers';
import TotalMessages from './TotalMessages';
import LastMessage from './LastMessage';
import getDateString from '../helpers/date'
class Header extends React.Component {

  getAllUsersCount() {
    const counts = {};
    this.props.messages.forEach(element => counts[element.user] = null);
    return Object.keys(counts).length;
  }

  getLastMessageTime() {
    const lengthMessagesArray = this.props.messages.length;
    const oneMessage = this.props.messages[lengthMessagesArray - 1];
    const date = (oneMessage) ? oneMessage.created_at : null;
    return getDateString(date);
  }

  render() {
    return (
      <header className='page-header'>
        <div className='header-chat-name'>
          myChat
        </div>
        <TotalUsers allUserCount={this.getAllUsersCount()} />
        <TotalMessages lengthMessages={this.props.messages.length} />
        <LastMessage lastMessageTime={this.getLastMessageTime()} />
      </header>
    )
  }
}

export default Header;