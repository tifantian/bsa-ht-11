import React from 'react';

class TotalUsers extends React.Component {

  render() {
    return (
      <div className='header-total-users'>
        {this.props.allUserCount} users
      </div>
    )
  }
}

export default TotalUsers;