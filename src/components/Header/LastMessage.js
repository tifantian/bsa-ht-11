import React from 'react';


class LastMessage extends React.Component {

  render() {
    return (
      <div className='header-last-message'>
        Last message {this.props.lastMessageTime}
      </div>
    )
  }
}

export default LastMessage;