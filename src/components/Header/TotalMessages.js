import React from 'react';

class TotalMessages extends React.Component {

  render() {
    return (
      <div className='header-total-messages'>
        {this.props.lengthMessages} messages
      </div>
    )
  }
}

export default TotalMessages;