import React from "react";
import Header from './Header/Header';
import MessageList from './MessageList/MessageList';
import MessageInput from './MessageInput/MessageInput';
import LoadingPage from './LoadingPage';
import 'bootstrap/dist/css/bootstrap.min.css';
import './stylesChat/reset.css'
import './stylesChat/styles.css';
import { connect } from 'react-redux';
import { fetchMessages, addMessage, deleteMessage, updateMessage } from '../store/actionsCreater'

class Chat extends React.Component {

  componentDidMount() {
    this.props.fetchMessages();
  }

  render() {
    if (this.props.messages.length) {
      return (
        <div className="Chat">
          <Header messages={this.props.messages} />
          <MessageList 
            messages={this.props.messages} 
            onMessageUpdate={(objectMessage) => this.props.updateMessage(objectMessage)}  
            onMessageDelete={(id) => this.props.deleteMessage(id)} 
          />
          <MessageInput onMessageSend={(messageObject) => this.props.addMessage(messageObject)} />
        </div>
      );
    } else {
      return <LoadingPage />
    }
  }
}

const putStateToProps = state => {
  return {
    messages: state
  }
};

const putDispatchToProps = {
  fetchMessages, 
  addMessage, 
  deleteMessage, 
  updateMessage
}

export default connect(putStateToProps, putDispatchToProps)(Chat);