import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import ChatComponent from './components/Chat';
import { rootReducer } from './store/reduser';
import thunk from 'redux-thunk';

const store = createStore(rootReducer, applyMiddleware(thunk));
const rootElement = document.getElementById('root');

ReactDOM.render(
  <Provider store={store}>
    <ChatComponent />
  </Provider> ,rootElement);
