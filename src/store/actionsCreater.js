import { FETCH_MESSAGES, ADD_MESSAGE, DELETE_MESSAGE, UPDATE_MESSAGE } from './actionsList';

export const fetchMessages = () => async dispatch => {
  const response = await fetch("https://api.myjson.com/bins/1hiqin").then(
    response => response.json()
  );
  dispatch({
    type: FETCH_MESSAGES,
    payload: response
  });
};

export const addMessage = (newMessage) => {
  return {
    type: ADD_MESSAGE,
    payload: newMessage
  }
};

export const deleteMessage = (id) => {
  return {
    type: DELETE_MESSAGE,
    payload: id
  }
}

export const updateMessage = (objectMessage) => {
  return {
    type: UPDATE_MESSAGE,
    payload: objectMessage
  }
}