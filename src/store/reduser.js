import { FETCH_MESSAGES, ADD_MESSAGE, DELETE_MESSAGE, UPDATE_MESSAGE } from "./actionsList";

const initialState = {
  messages: []
};

export const rootReducer = (state = initialState, action) => {
  
  switch (action.type) {
    case FETCH_MESSAGES:
      return action.payload; 
    case ADD_MESSAGE:
      return [...state, action.payload];
    case DELETE_MESSAGE:
      return [...state].filter(message => message.id !== action.payload);
    case UPDATE_MESSAGE:
      const {id} = action.payload;
      const updatedState = [...state].filter(message => message.id !== id);
      return [...updatedState, action.payload];
    default:
      return state;
  }
}

